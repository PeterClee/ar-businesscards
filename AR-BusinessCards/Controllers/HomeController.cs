﻿using AR_BusinessCards.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Anglia_Ruskin_Web_App.Controllers
{
    public class HomeController : Controller
    {

        public ActionResult Addrecord()
        {
            ViewBag.Message = "Adding Records";

            return View();
        }

        public ActionResult Updaterecord(int ChangeRecord)
        {
            ARBusinessCardDatabaseEntities DB = new ARBusinessCardDatabaseEntities();
            var Updatecompanydetails = 
                from CompanyTbl in DB.CompanyTbls
                where CompanyTbl.ComID == ChangeRecord
                select new { CompanyTbl.ComID, CompanyTbl.Com_Name, CompanyTbl.Com_Phone, CompanyTbl.Com_Email };

            return View(Updatecompanydetails);
        }

        public ActionResult Deleterecord()
        {
            ViewBag.Message = "Deleting Records";

            return View();
        }

        public ActionResult Index()
        {
            ARBusinessCardDatabaseEntities DB = new ARBusinessCardDatabaseEntities();
            List<CompanyTbl> companydetails = DB.CompanyTbls.ToList();
            //var BusinessCardDetails =
            //    from CompanyTbl in DB.CompanyTbls
            //    select new { CompanyTbl.ClientTbl.Client_Name, CompanyTbl.Com_Name, CompanyTbl.Com_Phone, CompanyTbl.Com_Email };


            return View(companydetails);
        }
    }
}