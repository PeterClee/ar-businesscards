﻿using AR_BusinessCards.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AR_BusinessCards.Controllers
{
    

    public class ARDatabaseController : Controller
    {
        // GET: ARDatabase
        public ActionResult Index()
        {
            ARBusinessCardDatabaseEntities DB = new ARBusinessCardDatabaseEntities();
            List<CompanyTbl> companydetails = DB.CompanyTbls.ToList();
            // var BusinessCardDetails = DB.Database.ExecuteSqlCommand("SELECT ClientTbl.Client_Name, CompanyTbl.Com_Name, CompanyTbl.Com_Phone, CompanyTbl.Com_Email FROM CompanyTbl INNER JOIN ClientTbl ON CompanyTbl.ClientID=ClientTbl.ClientID").ToString().ToList();


            return View(companydetails);
        }
    }
}